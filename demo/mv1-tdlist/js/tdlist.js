// 需求1:用vue渲染任务列表
// var list = [
//     {
//         // 任务标题
//         title:"吃饭睡觉打豆豆",
//         // 任务选中状态
//         isChecked:false
//     },
//     {
//         title:"好好学习,天天向上",
//         isChecked:true
//     }
// ]

// 需求8:本地存储数据

var store = {
    save: function(key, value) {
        localStorage.setItem(key, JSON.stringify(value));
    },
    fetch: function(key) {
        return JSON.parse(localStorage.getItem(key)) || [];
    }
}

var list = store.fetch("todolist");

var filtered = {
    // 全部任务
    all: function(list) {
        return list
    },
    // 未完成任务
    unfinish: function(list) {
        return list.filter(function(item) {
            return !item.isChecked
        })
    },
    // 已完成任务
    finish: function(list) {
        return list.filter(function(item) {
            return item.isChecked
        })
    }
}

// 需求9:②.vm实例化
var vm = new Vue({
    el: ".main",
    data: {
        tdlist: list,
        // 需求2:②.添加任务的内容存取
        todo: "",
        // 需求6:①.编辑任务-任务内容
        etodo: "",
        // 需求6:④.编辑前的任务标题
        beforeTodoTitle: "",
        // 需求9:②.记录hash值的状态,默认为all(全部任务)
        visibility: "all"
    },
    methods: {
        // 需求2: ①.添加任务
        addTodo: function() {
            if (this.todo) {
                this.tdlist.push({
                    title: this.todo,
                    isChecked: false
                })
                this.todo = "";
            } else {
                alert("请输入任务内容！");
            }
        },
        // 需求5:删除任务
        delTodo: function(item) {
            var delTrip = confirm("您真的要删除这条任务吗？");
            if (delTrip) {
                var index = this.tdlist.indexOf(item);
                this.tdlist.splice(index, 1);
            }
        },
        // 需求6:①.编辑任务--双击出现编辑框和任务内容
        editTodo: function(item) {
            this.beforeTodoTitle = item.title;

            this.etodo = item;
        },
        // 需求6:③.编辑任务-任务编辑完成
        editTodoSuccess: function() {
            this.etodo = "";
        },
        // 需求6:④.编辑任务-取消编辑
        editTodoCancel: function(item) {
            item.title = this.beforeTodoTitle;

            // 取消编辑状态
            this.etodo = "";

            this.beforeTodoTitle = ""
        }
    },
    directives: {
        // 自定义指令:自动获取焦点
        // 需求6:②.编辑任务-自动获取焦点
        "focus": {
            "update": function(el, binding) {
                // console.log(el);
                // console.log(binding);
                if (binding.value) {
                    el.focus();
                }
            }
        }
    },
    computed: {
        noTdlistLen: function() {
            return this.tdlist.filter(function(item) {
                return !item.isChecked
            }).length
        },
        hashListFilter: function() {
            /*var filtered = {
                // 全部任务
                all:function(list){
                    return list
                },
                // 未完成任务
                unfinish:function(list){
                    return list.filter(function(item){
                        return !item.isChecked
                    })
                },
                // 已完成任务
                finish:function(list){
                    return list.filter(function(item){
                        return item.isChecked
                    })
                }
            }*/

            // filtered[this.visibility](list);

            return filtered[this.visibility] ? filtered[this.visibility](list) : list
        }
    },
    watch: {
        // tdlist:function(){
        //     store.save("todolist",this.tdlist);
        // }
        tdlist: {
            handler: function() {
                store.save("todolist", this.tdlist)
            },
            deep: true
        }
    }
})

// 需求9:根据hash的状态,显示对应的状体数据列表

// 需求9:①.监听页面hash值的变化,并取得hash值
function getHash() {
    var urlHash = window.location.hash;
    var hash = urlHash.slice(1);

    if (hash == 'unfinish' || hash == 'finish') {
        vm.visibility = hash;
    } else {
        vm.visibility = 'all';
    }

    console.log(hash);
    // return hash;
}

getHash();

window.addEventListener("hashchange", getHash)